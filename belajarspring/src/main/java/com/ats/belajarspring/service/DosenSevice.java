package com.ats.belajarspring.service;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ats.belajarspring.model.DosenModel;
import com.ats.belajarspring.repository.DosenRepository;

@Service
@Transactional
public class DosenSevice {
	
	@Autowired
	private DosenRepository dosenRepository;
	
	//modifier kosong/gak kosong namaMethod(TipeVar namaVar)
	public void create(DosenModel dosenModel) {
		dosenRepository.save(dosenModel);
	}
	
	public List<DosenModel> read(){
		return dosenRepository.findAll();
	}
	
	public List<DosenModel> readOrderNama(){
		return dosenRepository.querySelectAllOrdeNamaDesc();
	}
	
	public List<DosenModel> readOrderBy(){
		return dosenRepository.querySelectAllOrdeUsiaDesc();
	}
	
	public List<DosenModel> readWhereNama(String katakunci_nama){
		return dosenRepository.querySelectAllWhereNamaLike(katakunci_nama);
	}
	
	public List<DosenModel> readWhereUsia(int katakunci_usia, String awalan){
		return dosenRepository.queryWhereUsiaEqual(katakunci_usia, awalan);
	}
	
	public List<DosenModel> usiaDuaLima(){
		return dosenRepository.usiaDuaLima();
	}
	
	public List<DosenModel> readWhereUsiaByOperator(int katakunci_usia, String katakunci_operator, String awalan){
		List<DosenModel>dosenModelList = new ArrayList<DosenModel>();
		
		if (katakunci_operator.equals("=")) {
			dosenModelList = dosenRepository.queryWhereUsiaEqual(katakunci_usia, awalan);
		}else if (katakunci_operator.equals(">")) {
			dosenModelList = dosenRepository.queryWhereUsiaMore(katakunci_usia, awalan);
		}else if (katakunci_operator.equals("<")) {
			dosenModelList = dosenRepository.queryWhereUsiaLess(katakunci_usia, awalan);
		}else if (katakunci_operator.equals(">=")) {
			dosenModelList = dosenRepository.queryWhereUsiaMoreEqual(katakunci_usia, awalan);
		}else if (katakunci_operator.equals("<=")) {
			dosenModelList = dosenRepository.queryWhereUsiaLessEqual(katakunci_usia, awalan);
		} else {
			dosenModelList = dosenRepository.queryWhereUsiaNotEqual(katakunci_usia, awalan);
		}
		
		return dosenModelList;
	}
	
	public DosenModel readByID(String namaDosen) {
		return dosenRepository.queryWhereNamaDosen(namaDosen);
	}
	
	public void delete(String namaDosen) {
		dosenRepository.deleteById(namaDosen);
	}
	
	
	
}
