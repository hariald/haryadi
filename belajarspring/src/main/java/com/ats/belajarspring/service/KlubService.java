package com.ats.belajarspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ats.belajarspring.model.KlubModel;
import com.ats.belajarspring.repository.KlubRepository;

@Service
@Transactional
public class KlubService {
	
	@Autowired
	private KlubRepository klubRepository;
	
	public void create(KlubModel klubModel) {
		klubRepository.save(klubModel);
	}
	
	public List<KlubModel> tampilKlub() {
		return klubRepository.findAll();
	}
	
	public List<KlubModel> urutJuara() {
		return klubRepository.jumlahProdukDesc();
	}
}
