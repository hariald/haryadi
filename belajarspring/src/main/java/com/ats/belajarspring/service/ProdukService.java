package com.ats.belajarspring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ats.belajarspring.model.ProdukModel;
import com.ats.belajarspring.repository.ProdukRepository;

@Service
@Transactional
public class ProdukService {
	
	@Autowired
	private ProdukRepository produkRepository;
	
	public void create(ProdukModel produkModel) {
		produkRepository.save(produkModel);
	}
	
	public List<ProdukModel> tampilProduk() {
		return produkRepository.findAll();
	}
	
	public List<ProdukModel> cariNama(String cari) {
		return produkRepository.cariNamaProduk(cari);
	}
	
	public List<ProdukModel> cariKode(String cari) {
		return produkRepository.cariKodeProduk(cari);
	}
	
	public List<ProdukModel> urutHargaAsc() {
		return produkRepository.hargaProdukAsc();
	}
	
	public void delete(String kode) {
		produkRepository.deleteById(kode);
	}

}
