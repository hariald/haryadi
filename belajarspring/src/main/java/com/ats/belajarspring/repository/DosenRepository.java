package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.DosenModel;

public interface DosenRepository extends JpaRepository<DosenModel, String> {

	@Query("select d from DosenModel d order by d.namaDosen DESC")
	List<DosenModel> querySelectAllOrdeNamaDesc();
	
	@Query("select d from DosenModel d order by d.namaDosen ASC")
	List<DosenModel> querySelectAllOrdeNamaAsc();
	
	@Query("select d from DosenModel d order by d.usiaDosen DESC")
	List<DosenModel> querySelectAllOrdeUsiaDesc();
	
	@Query("select d from DosenModel d order by d.usiaDosen ASC")
	List<DosenModel> querySelectAllOrdeUsiaAsc();
	
	@Query("select d from DosenModel d where d.namaDosen like %?1%")
	List<DosenModel> querySelectAllWhereNamaLike(String katakunci_nama);
	
	@Query("select d from DosenModel d where d.usiaDosen = ?1 and d.namaDosen like ?2%")
	List<DosenModel> queryWhereUsiaEqual(int katakunci_usia, String awalan);
	
	@Query("select d from DosenModel d where d.usiaDosen > ?1 and d.namaDosen like ?2%")
	List<DosenModel> queryWhereUsiaMore(int katakunci_usia, String awalan);
	
	
	@Query("select d from DosenModel d where d.usiaDosen < ?1 and d.namaDosen like ?2%")
	List<DosenModel> queryWhereUsiaLess(int katakunci_usia, String awalan);
	
	@Query("select d from DosenModel d where d.usiaDosen >= ?1 and d.namaDosen like ?2%")
	List<DosenModel> queryWhereUsiaMoreEqual(int katakunci_usia, String awalan);
	
	@Query("select d from DosenModel d where d.usiaDosen <= ?1 and d.namaDosen like ?2%")
	List<DosenModel> queryWhereUsiaLessEqual(int katakunci_usia, String awalan);
	
	@Query("select d from DosenModel d where d.usiaDosen != ?1 and d.namaDosen like ?2%")
	List<DosenModel> queryWhereUsiaNotEqual(int katakunci_usia, String awalan);
	
	@Query("select d from DosenModel d where d.namaDosen = ?1")
	DosenModel queryWhereNamaDosen(String namaDosen);
	
	@Query("select d from DosenModel d where d.usiaDosen > 25")
	List<DosenModel> usiaDuaLima();
	
}
