package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.KlubModel;

public interface KlubRepository extends JpaRepository<KlubModel, String> {
	
	
	@Query("select k from KlubModel k where k.namaKlub like '%r%' order by k.juara desc")
	List<KlubModel> jumlahProdukDesc();
}
