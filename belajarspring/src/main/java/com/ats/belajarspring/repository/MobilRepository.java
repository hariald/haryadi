package com.ats.belajarspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ats.belajarspring.model.MobilModel;

public interface MobilRepository extends JpaRepository<MobilModel, String> {
	
	

}
