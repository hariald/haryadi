package com.ats.belajarspring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ats.belajarspring.model.KarakterModel;

public interface KarakterRepository extends JpaRepository<KarakterModel, String> {

}
