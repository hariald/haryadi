package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ats.belajarspring.model.PresidenModel;

public interface PresidenRepository extends JpaRepository<PresidenModel, String> {
	
	@Query("select p from PresidenModel p where p.namaPresiden like '%b%' and p.usiaPresiden = 82")
	public List<PresidenModel> presiden();
}
