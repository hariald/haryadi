package com.ats.belajarspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.ProdukModel;

public interface ProdukRepository extends JpaRepository<ProdukModel, String> {
	
	
	@Query("select p from ProdukModel p where p.namaProduk like %?1%")
	List<ProdukModel> cariNamaProduk(String cari);
	
	@Query("select p from ProdukModel p where p.kodeProduk like %?1%")
	List<ProdukModel> cariKodeProduk(String cariKode);
	
	@Query("select p from ProdukModel p order by p.hargaProduk asc")
	List<ProdukModel> hargaProdukAsc();
}
