package com.ats.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_MOBIL")
public class MobilModel {
	
	@Id
	@Column(name = "MERK_MOBIL")
	private String merkMobil;
	
	@Column(name = "JUMLAH_MOBIL")
	private int jumlahMobil;

	public String getMerkMobil() {
		return merkMobil;
	}

	public void setMerkMobil(String merkMobil) {
		this.merkMobil = merkMobil;
	}

	public int getJumlahMobil() {
		return jumlahMobil;
	}

	public void setJumlahMobil(int jumlahMobil) {
		this.jumlahMobil = jumlahMobil;
	}
	
	

}
