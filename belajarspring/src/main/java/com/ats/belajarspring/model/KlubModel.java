package com.ats.belajarspring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TBL_KLUB_BOLA")
public class KlubModel {
	
	@Id
	@Column(name = "KODE_KLUB")
	private String kodeKlub;
	
	@Column(name = "NAMA_KLUB")
	private String namaKlub;
	
	@Column(name = "JUARA")
	private int juara;
	
	@Column(name = "FANBASE")
	private int fanbase;

	public String getKodeKlub() {
		return kodeKlub;
	}

	public void setKodeKlub(String kodeKlub) {
		this.kodeKlub = kodeKlub;
	}

	public String getNamaKlub() {
		return namaKlub;
	}

	public void setNamaKlub(String namaKlub) {
		this.namaKlub = namaKlub;
	}

	public int getJuara() {
		return juara;
	}

	public void setJuara(int juara) {
		this.juara = juara;
	}

	public int getFanbase() {
		return fanbase;
	}

	public void setFanbase(int fanbase) {
		this.fanbase = fanbase;
	}
	

}
