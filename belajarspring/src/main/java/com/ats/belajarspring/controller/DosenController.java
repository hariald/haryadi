package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.DosenModel;
import com.ats.belajarspring.service.DosenSevice;

@Controller
public class DosenController {
	
	@Autowired // Pengganti Instance = IOC/DI
	private DosenSevice dosenService;
	

	
	@RequestMapping(value = "tambah_dosen")
	public String menuIsidosen() {
		String html = "dosen/isi_dosen";
		return html;
	}
	
	@RequestMapping(value = "hasil_satu_dosen")
	public String menuSatudosen(HttpServletRequest request, Model model) {
		
		String namaDosen = request.getParameter("nama");
		int usiaDosen = Integer.valueOf(request.getParameter("usia"));
		
		DosenModel dosenModel = new DosenModel();
		dosenModel.setNamaDosen(namaDosen);
		dosenModel.setUsiaDosen(usiaDosen);
		
		//transaksi simpan C = create
		dosenService.create(dosenModel);
		readData(model);
		
		model.addAttribute("dosenModel", dosenModel);
		
		String html = "dosen/banyak_dosen";
		return html;
	}
	
	
	@RequestMapping(value = "hasil_banyak_dosen")
	public String menuBanyakDosen( Model model) {
		
		List<DosenModel>dosenModelList = new ArrayList<>();
		dosenModelList = dosenService.readOrderNama();
		model.addAttribute("dosenModelList", dosenModelList);;
		
		
		String html = "dosen/banyak_dosen";
		return html;
	}
	
	@RequestMapping(value = "hasil_banyak_dosen_order_nama")
	public String menuBanyakDosenOrderNama( Model model) {
		
		List<DosenModel>dosenModelList = new ArrayList<>();
		dosenModelList = dosenService.readOrderNama();
		model.addAttribute("dosenModelList", dosenModelList);
		
		
		String html = "dosen/banyak_dosen_order_nama";
		return html;
	}
	
	@RequestMapping(value = "order_dosen")
	public String menuOrderDosen( Model model) {
		
		List<DosenModel>dosenModelList = new ArrayList<>();
		dosenModelList = dosenService.readOrderBy();
		model.addAttribute("dosenModelList", dosenModelList);
		
		
		String html = "dosen/order_dosen";
		return html;
	}
	
	@RequestMapping(value = "cari_nama_dosen")
	public String menuCariNamaDosen( Model model) {
		
		List<DosenModel>dosenModelList = new ArrayList<>();
		dosenModelList = dosenService.readOrderBy();
		model.addAttribute("dosenModelList", dosenModelList);
		
		
		String html = "dosen/cari_nama_dosen";
		return html;
	}
	
	@RequestMapping(value = "proses_cari_nama")
	public String menuProsesCariNamaDosen( Model model, HttpServletRequest request) {
		String katakunci_nama = request.getParameter("katakunci_nama");
		
		List<DosenModel>dosenModelList = new ArrayList<>();
		dosenModelList = dosenService.readWhereNama(katakunci_nama);
		model.addAttribute("dosenModelList", dosenModelList);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		

		
		String html = "dosen/cari_nama_dosen";
		return html;
	}
	
	
	@RequestMapping(value = "cari_usia")
	public String menuCariUsia( Model model) {
		
		List<DosenModel>dosenModelList = new ArrayList<>();
		dosenModelList = dosenService.readOrderBy();
		model.addAttribute("dosenModelList", dosenModelList);
		
		
		String html = "dosen/cari_usia";
		return html;
	}
	
	@RequestMapping(value = "proses_cari_usia")
	public String menuProsesCariUsia( Model model, HttpServletRequest request) {
		int katakunci_usia = Integer.valueOf(request.getParameter("katakunci_usia"));
		String katakunci_operator = request.getParameter("operator");
		String awalan = request.getParameter("awalan");
		
		List<DosenModel>dosenModelList = new ArrayList<>();
		dosenModelList = dosenService.readWhereUsiaByOperator(katakunci_usia, katakunci_operator, awalan);
		model.addAttribute("dosenModelList", dosenModelList);
		
		
		String html = "dosen/cari_usia";
		return html;
	}
	
	@RequestMapping(value = "ubah_dosen")
	public String menuUbahDosen( Model model, HttpServletRequest request) {
		String namaID = request.getParameter("namaID");
		
		DosenModel dosenModel = new DosenModel();
		dosenModel = dosenService.readByID(namaID);
		model.addAttribute("dosenModel", dosenModel);
		
		String html = "dosen/ubah_dosen";
		return html;
	}

	@RequestMapping(value = "hapus_dosen")
	public String menuHapusDosen( Model model, HttpServletRequest request) {
		String namaID = request.getParameter("namaID");
		
		DosenModel dosenModel = new DosenModel();
		dosenModel = dosenService.readByID(namaID);
		model.addAttribute("dosenModel", dosenModel);
		
		String html = "dosen/hapus_dosen";
		return html;
	}
	
	@RequestMapping(value = "proses_hapus_dosen")
	public String menuProsesHapusDosen (HttpServletRequest request, Model model) {
		String namaID = request.getParameter("namaID");
		dosenService.delete(namaID);
		
		readData(model);
		
		String html = "dosen/banyak_dosen";
		return html;
		
	}
	 
	@RequestMapping(value = "proses_ubah_dosen")
	public String menuUbahdosen(HttpServletRequest request, Model model) {
		
		String namaDosen = request.getParameter("namaID");
		int usiaDosen = Integer.valueOf(request.getParameter("usiaID"));
		
		DosenModel dosenModel = new DosenModel();
		dosenModel.setNamaDosen(namaDosen);
		dosenModel.setUsiaDosen(usiaDosen);
		
		
		dosenService.create(dosenModel);
		
		readData(model);
		
		
		String html = "dosen/banyak_dosen";
		return html;
	}
	
	public void readData(Model model) {
		List<DosenModel>dosenModelList = new ArrayList<>();
		dosenModelList = dosenService.readOrderNama();
		model.addAttribute("dosenModelList", dosenModelList);
	}
	
}
