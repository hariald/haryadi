package com.ats.belajarspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.ats.belajarspring.model.ProdukModel;
import com.ats.belajarspring.service.ProdukService;

import net.bytebuddy.asm.Advice.Return;

@RestController
@RequestMapping("/produk")
public class ProdukApi {
	
	@Autowired
	private ProdukService produkService;
	
	@PostMapping("/create")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Map<String, Object> post(@RequestBody ProdukModel produkModel) {
		produkService.create(produkModel);
		
		Map<String, Object>map = new HashMap<String, Object>();
		map.put("Pesan", "Data Berhasil Ditambahkan");
		return map;
	}
	
	@GetMapping("/read")
	public List<ProdukModel> get() {
		List<ProdukModel>produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.tampilProduk();
		return produkModelList;
	}
	
	@PutMapping("/update")
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, Object> put(@RequestBody ProdukModel produkModel) {
		produkService.create(produkModel);
		
		Map<String, Object>map = new HashMap<String, Object>();
		map.put("Pesan", "Data Berhasil Diupdate");
		return map;
	}
	
	@DeleteMapping("/delete/{kodeProduk}")
	@ResponseStatus(code = HttpStatus.GONE)
	public Map<String, Object> delete(@PathVariable String kodeProduk) {
		produkService.delete(kodeProduk);
		
		Map<String, Object>map = new HashMap<String, Object>();
		map.put("Pesan", "Data Berhasil Dihapus");
		return map;
	}

}
