package com.ats.belajarspring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.KarakterModel;

@Controller
public class KarakterController {
	
	@RequestMapping(value = "tambahkarakter")
	public String menuKarakter() {
		String html = "karakter/isi_karakter";
		return html;
	}
	
	@RequestMapping(value = "satu_karakter")
	public String satuKarakter(HttpServletRequest request, Model model) {
		
		String nama = request.getParameter("namaKarakter");
		int level = Integer.valueOf(request.getParameter("levelKarakter"));
		String Status = request.getParameter("statusKarakter");
		
		KarakterModel karakterModel = new KarakterModel();
		karakterModel.setNama(nama);
		karakterModel.setLevel(level);
		karakterModel.setStatus(Status);
		
		model.addAttribute("karakterModel", karakterModel);
		
		String html = "karakter/satu_karakter";
		return html;
	}

}
