package com.ats.belajarspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ats.belajarspring.model.PresidenModel;
import com.ats.belajarspring.service.PresidenService;

@RestController
@RequestMapping("/presiden")
public class PresidenApi {
	
	@Autowired
	private PresidenService presidenService;
	
	@PostMapping("/create")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Map<String, Object> create(@RequestBody PresidenModel presidenModel) {
		presidenService.create(presidenModel);
		
		Map<String, Object>map = new HashMap<String, Object>();
		map.put("Pesan", "Data Berhasil Ditambahkan");
		return map;
	}
	
	@GetMapping("/read")
	public List<PresidenModel> read() {
		List<PresidenModel> presidenModelList = new ArrayList<PresidenModel>();
		presidenModelList = presidenService.read();
		return presidenModelList;
	}
	
	@GetMapping("/usia")
	public List<PresidenModel> usia() {
		List<PresidenModel> presidenModelList = new ArrayList<PresidenModel>();
		presidenModelList = presidenService.usia();
		return presidenModelList;
	}

}
