package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.MobilModel;
import com.ats.belajarspring.service.MobilService;

@Controller
public class MobilController {
	
	@Autowired
	private MobilService mobilService;
	
	@RequestMapping(value = "mobil")
	public String menuMobil() {
		String html = "mobil/daftarmobil";
		return html;
	}
	
	@RequestMapping(value = "hasil_isi_mobil")
	public String menuIsiMobil(HttpServletRequest request, Model model) {
		String merkMobil = request.getParameter("merk");
		int jumlahMobil = Integer.valueOf(request.getParameter("jumlah"));
		
		MobilModel mobilModel = new MobilModel();
		mobilModel.setMerkMobil(merkMobil);
		mobilModel.setJumlahMobil(jumlahMobil);
		
		mobilService.create(mobilModel);
		
		List<MobilModel>mobilModelList = new ArrayList<MobilModel>();
		mobilModelList = mobilService.read();
		model.addAttribute("mobilModelList", mobilModelList);
		
		String html = "mobil/isimobil";
		return html;
	}

}
