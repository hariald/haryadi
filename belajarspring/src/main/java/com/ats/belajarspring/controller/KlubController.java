package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.KlubModel;
import com.ats.belajarspring.service.KlubService;

@Controller
public class KlubController {
	
	@Autowired
	private KlubService klubService;
	
	@RequestMapping(value = "tambah_klub")
	public String menuIsiKlub() {
		String html = "klub_bola/isi_klub_bola";
		return html;
	}
	
	@RequestMapping(value = "tampil_klub")
	public String menuTampilKlub(HttpServletRequest request, Model model) {
		String kode = request.getParameter("kode");
		String nama = request.getParameter("nama");
		int juara = Integer.valueOf(request.getParameter("juara"));
		int fanbase = Integer.valueOf(request.getParameter("fanbase"));
		
		KlubModel klubModel = new KlubModel();
		klubModel.setKodeKlub(kode);
		klubModel.setNamaKlub(nama);
		klubModel.setJuara(juara);
		klubModel.setFanbase(fanbase);
		
		klubService.create(klubModel);
		
		List<KlubModel>klubModelList = new ArrayList<KlubModel>();
		klubModelList = klubService.tampilKlub();
		model.addAttribute("klubModelList", klubModelList);
		
		String html = "klub_bola/tampil_klub";
		return html;
	}
	
	@RequestMapping(value = "daftar_klub")
	public String menuDaftaKlub(Model model) {
		
		List<KlubModel>klubModelList = new ArrayList<KlubModel>();
		klubModelList = klubService.urutJuara();
		model.addAttribute("klubModelList", klubModelList);
		
		String html = "klub_bola/daftar_klub";
		return html;
	}

}
