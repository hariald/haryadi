package com.ats.belajarspring.controller.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ats.belajarspring.model.DosenModel;
import com.ats.belajarspring.repository.DosenRepository;
import com.ats.belajarspring.service.DosenSevice;

@RestController
@RequestMapping("/api/DosenApi")
public class DosenApi {
	
	@Autowired
	private DosenSevice dosenService;
	
	@Autowired
	private DosenRepository dosenRepository;
	
	
	@PostMapping("/post")
	@ResponseStatus(code = HttpStatus.CREATED)
	public Map<String, Object> postapi(@RequestBody DosenModel dosenModel){
		this.dosenService.create(dosenModel);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Successs", Boolean.TRUE);
		map.put("Pesan", "Selamat data berhasil dimasukkan");
		return map;
	}
	
	@GetMapping("/get")
	@ResponseStatus(code = HttpStatus.OK)
	public List<DosenModel> getapi(){
		List<DosenModel>dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.read();
		return dosenModelList;
	}
	
	@GetMapping("/usiadualima")
	@ResponseStatus(code = HttpStatus.OK)
	public List<DosenModel> usiaDuaLima(){
		List<DosenModel>dosenModelList = new ArrayList<DosenModel>();
		dosenModelList = this.dosenService.usiaDuaLima();
		return dosenModelList;
	}
	
	@PutMapping("/put")
	@ResponseStatus(code = HttpStatus.OK)
	public Map<String, Object> putapi(@RequestBody DosenModel dosenModel) {
		this.dosenService.create(dosenModel);
		
		Map<String, Object>map = new HashMap<String, Object>();
		map.put("Success", Boolean.TRUE);
		map.put("Pesan", "Data Diganti");
		return map;
	}
	
	@DeleteMapping("/delete/{namaDosen}")
	@ResponseStatus(code = HttpStatus.GONE	)
	public Map<String, Object> deleteapi(@PathVariable String namaDosen) {
		this.dosenService.delete(namaDosen);
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("Success", Boolean.TRUE);
		map.put("success", "Selamat Data anda "+namaDosen+" Telah Hilang");
		return map;
	}
	
	

}
