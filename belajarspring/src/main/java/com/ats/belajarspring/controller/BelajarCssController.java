package com.ats.belajarspring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class BelajarCssController {
	
	@RequestMapping(value = "css")
	public String css() {
		String html = "/belajarcss/contohcss";
		return html;
	}

}
