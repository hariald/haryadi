package com.ats.belajarspring.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TrisaktiController {
	
	@RequestMapping(value = "daftar")
	public String menuDaftar() {
		String html = "/trisakti/daftar";
		return html;
	}
	
	@RequestMapping(value = "hasil")
	public String menuHasil(HttpServletRequest request, Model model) {
		
		String nama = request.getParameter("nama_trisakti");
		String tempat = request.getParameter("tempat_lahir");
		String tanggal = request.getParameter("tanggal_lahir");
		String jk = request.getParameter("jenis_kelamin");
		String agama = request.getParameter("agama");
		String telp = request.getParameter("telp");
		String hp = request.getParameter("hp");
		String kwn = request.getParameter("kewarganegaraan");
		String nik = request.getParameter("nik");
		String gDarah = request.getParameter("gol_darah");
		String tinggal = request.getParameter("tinggal");
		String anak = request.getParameter("anak");
		String saudara = request.getParameter("saudara");
		String status = request.getParameter("status");
		
		model.addAttribute("hasil_nama", nama);
		model.addAttribute("hasil_tempat", tempat);
		model.addAttribute("hasil_tanggal", tanggal);
		model.addAttribute("hasil_jk", jk);
		model.addAttribute("hasil_agama", agama);
		model.addAttribute("hasil_telp", telp);
		model.addAttribute("hasil_hp", hp);
		model.addAttribute("hasil_kwn", kwn);
		model.addAttribute("hasil_nik", nik);
		model.addAttribute("hasil_gDarah", gDarah);
		model.addAttribute("hasil_tinggal", tinggal);
		model.addAttribute("hasil_anak", anak);
		model.addAttribute("hasil_saudara", saudara);
		model.addAttribute("hasil_status", status);
		
		String html = "/trisakti/hasil";
		return html;
	}

}
