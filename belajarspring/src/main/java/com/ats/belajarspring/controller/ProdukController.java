package com.ats.belajarspring.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ats.belajarspring.model.ProdukModel;
import com.ats.belajarspring.service.ProdukService;

@Controller
public class ProdukController {
	
	@Autowired
	private ProdukService produkService;
	
	@RequestMapping(value = "menu_tambah_produk")
	public String menuTambahProduk() {
		String html = "produk/tambah_produk";
		return html;
	}
	
	@RequestMapping(value = "proses_tambah_produk")
	public String prosesTambahProduk(HttpServletRequest request, Model model) {
		String kode = request.getParameter("kode");
		String nama = request.getParameter("nama");
		int harga = Integer.valueOf(request.getParameter("harga"));
		
		ProdukModel produkModel = new ProdukModel();
		produkModel.setNamaProduk(nama);
		produkModel.setKodeProduk(kode);
		produkModel.setHargaProduk(harga);
		
		produkService.create(produkModel);
		
		model.addAttribute("produkModel", produkModel);
		
		String html ="produk/success";
		return html;
	}
	
	@RequestMapping("tampil_produk")
	public String tampilProduk(Model model, HttpServletRequest request) {
		String kode = request.getParameter("kode");
		String nama = request.getParameter("nama");
		int harga = Integer.valueOf(request.getParameter("harga"));
		
		ProdukModel produkModel = new ProdukModel();
		produkModel.setNamaProduk(nama);
		produkModel.setKodeProduk(kode);
		produkModel.setHargaProduk(harga);
		
		produkService.create(produkModel);
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.tampilProduk();
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/tampilProduk";
		return html;
	}
	
	
	@RequestMapping("proses_cari")
	public String cariProduk(HttpServletRequest request, Model model) {
		String cari = request.getParameter("cariNama");
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.cariNama(cari);
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/tampilProduk";
		return html;
		
	}
	
	@RequestMapping("proses_cariKode")
	public String cariKodeProduk(HttpServletRequest request, Model model) {
		String cari = request.getParameter("cariKode");
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.cariKode(cari);
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/tampilProduk";
		return html;
		
	}
	
	@RequestMapping("daftar_produk")
	public String menuDaftarProduk(Model model) {
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.tampilProduk();
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/daftar_produk";
		return html;
	}
	
	@RequestMapping("menu_urut_asc_produk")
	public String menuUrustAscProduk(Model model) {
		
		List<ProdukModel> produkModelList = new ArrayList<ProdukModel>();
		produkModelList = produkService.urutHargaAsc();
		model.addAttribute("produkModelList", produkModelList);
		
		String html = "produk/urut_asc_produk";
		return html;
	}
}
